package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }


    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    Random random = new Random();
    String yourChoice;


    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    int randomChoice = random.nextInt(rpsChoices.size());
    public void run() {
        while (true) {
            System.out.println("Let's play round " + roundCounter);
            while (true) {
                System.out.println("Your choice (Rock/Paper/Scissors)?");
                yourChoice = sc.nextLine();
                if (rpsChoices.contains(yourChoice)) {
                    break;
                } else {
                    System.out.println("I do not understand " + yourChoice + ". Could you try again?");
                }
            }

            String computerChoice;
            if (randomChoice == 0) {
                computerChoice = "rock";
            } else if (randomChoice == 1) {
                computerChoice = "paper";
            } else {
                computerChoice = "scissors";
            }


            if (yourChoice.equals(computerChoice)) {
                System.out.println("Human chose " + yourChoice + ", computer chose " + computerChoice + ". It's a tie!");
            } else if (yourChoice.equals("rock") && computerChoice.equals("scissors")) {
                System.out.println("Human chose rock, computer chose scissors. Human wins!");
                humanScore++;
            } else if (yourChoice.equals("rock") && computerChoice.equals("paper")) {
                System.out.println("Human chose rock, computer chose paper. Computer wins!");
                computerScore++;
            } else if (yourChoice.equals("paper") && computerChoice.equals("rock")) {
                System.out.println("Human chose paper, computer chose rock. Human wins!");
                humanScore++;
            } else if (yourChoice.equals("paper") && computerChoice.equals("scissors")) {
                System.out.println("Human chose paper, computer chose scissors. Computer wins!");
                computerScore++;
            } else if (yourChoice.equals("scissors") && computerChoice.equals("rock")) {
                System.out.println("Human chose scissors, computer chose rock. Computer wins!");
                computerScore++;
            } else if (yourChoice.equals("scissors") && computerChoice.equals("paper")) {
                System.out.println("Human chose scissors, computer chose paper. Human wins!");
                humanScore++;
            }

            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            System.out.println("Do you wish to continue playing? (y/n)?");
            String continuee = sc.nextLine();
            if (continuee.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }
            roundCounter++;
        }

    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput (String prompt){
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}

